# README #

Repository per i progetti di noise removal da immagini mediche
EICA@UNICZ

### What is this repository for? ###

per ogni immagine nella cartella /images contrassegnata con il suffisso "noise" applicare i vari metodi di denoising a disposizione e confrontare il risultato, in maniera quantitava, per ogni risultato ottenuto con l'immagine di controllo contrassegnata con il suffisso "clean"
